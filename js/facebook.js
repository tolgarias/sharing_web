var access_token;
var loggedIn = false;
var userID;
window.fbAsyncInit = function() {
    FB.init({
      appId      : '660924587388321',
      xfbml      : true,
      version    : 'v2.6',
      status     : true
    });
    FB.getLoginStatus(function(response){
      statusChangeCallback(response);
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  function statusChangeCallback(response){
    if(response.status === 'connected'){
        access_token = response.authResponse.accessToken;
        userID = response.authResponse.userID;
        loggedIn = true;
    }
    else {
        access_token = '';
        loggedIn = false;
    }
  }
  function checkLoginState(){
    FB.getLoginStatus(function(response){
      statusChangeCallback(response);
    });
  }
  function getUserInfo(){
    FB.api('/me',function(response){
      console.log(response.name);
    });
  }
  function isLoggedIn(){
    return loggedIn;
  }
