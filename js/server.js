var app = angular.module('sharingApp',['ngTagsInput']);
app.config( [
    '$compileProvider',
    function( $compileProvider )
    {   
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension|spotify):/);
        // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
    }
]);

app.config(['$locationProvider',function($locationProvider) {
        $locationProvider.html5Mode(
        	{enabled:true,requireBase:false}
        	).hashPrefix('*');
    }
    ]);

app.controller('indexController',function($scope,$http,$sce,$location){
	//opening page
	var serverURL = "www.spotthelist.com";
	$scope.includeFile = "main.html";
	$scope.listData = {};
	$scope.listMode = 0;
	$scope.page = 1;		
	$scope.tagName = '';
	$scope.uid = '';
	$scope.location = $location.search();
	


	$scope.addList = function() {
		if(isLoggedIn()){
			var data = {
				spotifyUri:$scope.spotifyUri,
				name:$scope.name,
				tags:$scope.tags
			};
			
			$http.post("http://"+serverURL+":8080/lists/add?access_token="+access_token,$scope.listData).then(
				function(response){
					$scope.openRecentLists();
					showSuccesssNotification("your list has been added");
					$scope.listData = {};
				}
			);
		} else {
			showErrorNotification('Please login with Facebook!');
		}
	};

	$scope.addCommentToList = function(listId,comment) {
		if(isLoggedIn()){
			var data = {
				'comment':comment,
			};
			
			$http.post("http://"+serverURL+":8080/list/comments/add/"+listId+"?access_token="+access_token,data).then(
				function(response){
					$scope.getTheList(listId);
					showSuccesssNotification("your comment has been received");
				}
			);
		} else {
			showErrorNotification('Please login with Facebook!');
		}
	};

	//for the tags input
	$scope.getTagsFromService = function(query){
		return $http.get("http://"+serverURL+":8080/tags?query="+query);
	};
	
	//for the tags page
	$scope.getTags = function(){
		$http.get("http://"+serverURL+":8080/tags").then(
			function(response){
				$scope.all_tags = response.data;
			}
		);
	}

	$scope.openAddList = function(){
		$scope.page=1;
		$scope.openPage('add_list.html');
	}
	$scope.openPage = function(pageName){
		$scope.includeFile = pageName;
	}
	$scope.openTags = function(){
		$scope.getTags();
		$scope.openPage('tags.html');
		$scope.page=1;
	}
	$scope.openRecentLists = function(){
		$scope.page=1;
		$scope.listMode = 0;
		$scope.getRecentLists();
		$scope.openPage('main.html');
	}
	$scope.getRecentLists = function(){
		var _url  =  "http://"+serverURL+":8080/lists/all/bydate";
		$http({method: 'GET', url: _url, params: {'p':$scope.page}}).success(
				function(data,status,headers,config){
					$scope.pageCount = headers('page_count');
					$scope.lists = data;
					$scope.openPage('main.html');
				}
		);
	}
	$scope.createListUrlToEmbed = function(spotifyUri){
		return $sce.trustAsResourceUrl('https://embed.spotify.com/?uri='+spotifyUri);
	}

	$scope.createListUrlToOpen = function(spotifyUri){
		return $sce.trustAsResourceUrl(spotifyUri);
	}
	
	$scope.getListsByTag = function(tagName){
		$scope.tagName = tagName;
		var _url = "http://"+serverURL+":8080/lists/"+tagName;
		$http({method: 'GET', url: _url, params: {'p':$scope.page}}).success(
				function(data,status,headers,config){
					$scope.pageCount = headers('page_count');
					$scope.lists = data;
					$scope.openPage('main.html');
					$scope.listMode =1;
				}
		);
	}
	$scope.openListsByUser=function(uid){
		$scope.uid = uid;
		$scope.page = 1;
		$scope.getListsByUser($scope.uid);
	}
	$scope.getListsByUser = function(uid) {
		var _url = "http://"+serverURL+":8080/lists/byuser/"+uid;
		$http({method: 'GET', url: _url, params: {'p':$scope.page}}).success(
				function(data,status,headers,config){
					$scope.pageCount = headers('page_count');
					$scope.lists = data;
					$scope.openPage('main.html');
					$scope.listMode = 2;
				}
		);
	}
	$scope.gotoPage = function(pageIndex){
		$scope.page = pageIndex;
		if($scope.listMode == 0) {
			$scope.getRecentLists();
		}
		else if($scope.listMode == 1) {
			$scope.getListsByTag($scope.tagName);
		}
		else if($scope.listMode == 2) {
			$scope.getListsByUser($scope.uid);
		}
	}
	$scope.getTheList = function(listId){
		var _url = "http://"+serverURL+":8080/list/"+listId;
		$http({method: 'GET', url: _url, params: {}}).success(
				function(data,status,headers,config){
					$scope.spotifyList = data;
					$scope.openPage('list_info.html');
				}
		);
	}
	$scope.rateTheList = function(listId,rating){
		if(isLoggedIn()){
			var _url = "http://"+serverURL+":8080/list/rate/"+listId+"/"+rating+"?access_token="+access_token;
			$http({method: 'GET', url:_url, params: {}}).success(
				function(data,status,headers,config){
					$scope.pageCount = 1;
					$scope.listMode =0;
					$scope.getRecentLists();
					showSuccesssNotification('your rating has been received');
				}
			).error(
				function(data,status,headers,config){
					showErrorNotification('you already rated this list');	
				}
			);
		} else {
			showErrorNotification('Please login with Facebook!');
		}
	}
	$scope.isOwner = function(uid){
		return userID == uid;
	}
	$scope.editList=function(list){
		$scope.listData = {
			name :list.name,
			spotifyUri:list.spotifyUri,
			tags:list.tags
		};
		$scope.openAddList();
	}

	$scope.deleteList = function(listId){
		if(isLoggedIn()){
			var _url = "http://"+serverURL+":8080/list/delete/"+listId+"?access_token="+access_token;
			$http({method: 'GET', url:_url, params: {}}).success(
				function(data,status,headers,config){
					$scope.pageCount = 1;
					$scope.listMode =0;
					$scope.getRecentLists();
					showSuccesssNotification('your list is deleted');
				}
			)
		} else {
			showErrorNotification('Please login with Facebook!');
		}
	}
	$scope.searchLists = function(searchText){
			$http.post("http://"+serverURL+":8080/lists/search",searchText).then(
				function(response){
					$scope.lists = response.data;
					$scope.openPage('main.html');
				}
			);
	}
	
	//get data for the opening page
	if($scope.location['uid']!=null){
		$scope.openListsByUser($scope.location['uid']);
	}
	else if($scope.location['listId']!=null){
		$scope.getTheList($scope.location['listId']);
	}
	else if($scope.location['tag']!=null){
		$scope.getListsByTag($scope.location['tag']);
	}
	else {
		$scope.getRecentLists();
	}

}
);

app.filter('starRange',function() {
	return function(input,start,range){
		range = parseInt(range);
		for(var i=start;i<range;i++){
			input.push(i);
		}
		return input;
	};
}
);

